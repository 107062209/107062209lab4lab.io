function mykey(event){
    var keyin = event.key;
    if(keyin=="Enter"){
        solve();
    }
    else if(keyin=="Backspace"){
        var str = document.getElementById("screen").value;
        if(str.length == 1){
            document.getElementById("screen").value = "0";
        }
        else
            document.getElementById("screen").value = str.slice(0,-1);
    }
    else{
        dis(keyin);
    }
}

function dis(val){
    if(val == '0'){
        if(document.getElementById("screen").value === '0'){
            document.getElementById("screen").value = '0';
        }
        else{
            document.getElementById("screen").value+=val;    
        }
    }
    else{
        if(document.getElementById("screen").value === '0'){
            if(val == '.')
                document.getElementById("screen").value += val;
            else
                document.getElementById("screen").value = val;
        }
        else{
            document.getElementById("screen").value+=val;
        }
    }
}

function clr(){
    document.getElementById("screen").value = "0";
}

function solve(){
    try{
        var x=eval(document.getElementById("screen").value);
        document.getElementById("screen").value = x;
    }
    catch(err){
        document.getElementById("screen").value = "error";
    }
}